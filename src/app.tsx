import { FormsPage } from './components';

export function App() {
    return (
        <main>
            <h1>Introducción a TS - React</h1>
            <FormsPage />
        </main>
    );
}
