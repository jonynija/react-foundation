import { useState } from 'preact/hooks';

export const Counter = () => {
    const [count, setCount] = useState<number>(10);

    const increaseBy = (value: number): void => {
        setCount(count + value);
    };

    const decreaseBy = (value: number): void => {
        setCount(count - value);
    };

    return (
        <>
            <h3>
                Contador: <small>{count}</small>
            </h3>

            <div>
                <button onClick={() => increaseBy(1)}>+1</button>
                &nbsp;
                <button onClick={() => decreaseBy(1)}>-1</button>
            </div>
        </>
    );
};
