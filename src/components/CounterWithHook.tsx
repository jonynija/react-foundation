import { UseCounter } from '../hooks/UseCounter';

export const CounterWithHook = () => {
    const { count, increaseBy, decreaseBy } = UseCounter({});

    return (
        <>
            <h3>
                Contador: <small>{count}</small>
            </h3>

            <div>
                <button onClick={() => increaseBy(1)}>+1</button>
                &nbsp;
                <button onClick={() => decreaseBy(1)}>-1</button>
            </div>
        </>
    );
};
