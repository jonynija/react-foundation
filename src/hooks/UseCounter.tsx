import { useState } from 'preact/hooks';

interface Options {
    initValue?: number;
}

export const UseCounter = ({ initValue = 0 }: Options) => {
    const [count, setCount] = useState<number>(initValue);

    const increaseBy = (value: number): void => {
        setCount(count + value);
    };

    const decreaseBy = (value: number): void => {
        const newValue = count - value;

        if (newValue <= 0) return;

        setCount(count - value);
    };

    return {
        // Properties
        count,

        // Methods
        increaseBy,
        decreaseBy,
    };
};
