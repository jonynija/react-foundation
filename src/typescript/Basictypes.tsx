export const Basictypes = () => {
    const name: string = 'Jonathan';
    const age: number = 22;
    const isActive: boolean = false;

    const powers: string[] = ['React', 'Flutter'];

    return (
        <>
            <h3>Tipos Basicos</h3>

            {name}
            {age}
            {isActive ? 'Activo' : 'Inactivo'}

            <br />

            {powers.join(', ')}
        </>
    );
};
