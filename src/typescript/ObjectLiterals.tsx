interface Person {
    firstName: string;
    lastName: string;
    age: number;
    address: Address;
}

interface Address {
    country: string;
    city: string;
}

export const ObjectLiterals = () => {
    const person: Person = {
        firstName: 'Jonathan',
        lastName: 'Ixcayau',
        age: 22,
        address: {
            country: 'Argentina',
            city: 'Buenos Aires',
        },
    };

    return (
        <>
            <h3>Objetos Literales</h3>
            <pre>{JSON.stringify(person, null, 2)}</pre>
        </>
    );
};
